import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-horizontal-scrollview',
  templateUrl: './horizontal-scrollview.component.html',
  styleUrls: ['./horizontal-scrollview.component.scss']
})
export class HorizontalScrollviewComponent implements OnInit {

  data;
  quantity: number = 0;
  @Input() trendingproducts;
  @Output() dataEvent = new EventEmitter();
  @Output() sendproductincart = new EventEmitter();
  constructor() { 
    
  }

  ngOnInit(): void {
  }

  receivedata($event){
    this.data = $event;
    this.dataEvent.emit(this.data);
   
    
  }

  userproduct($event){
  
    this.sendproductincart.emit($event);
  }

}
