import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HorizontalScrollviewComponent } from './horizontal-scrollview.component';

describe('HorizontalScrollviewComponent', () => {
  let component: HorizontalScrollviewComponent;
  let fixture: ComponentFixture<HorizontalScrollviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HorizontalScrollviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HorizontalScrollviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
