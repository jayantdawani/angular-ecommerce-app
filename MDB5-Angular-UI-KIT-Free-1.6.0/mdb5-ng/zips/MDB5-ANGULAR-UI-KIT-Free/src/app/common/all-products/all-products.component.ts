import { Component, Input, OnInit ,Output , EventEmitter } from '@angular/core';

@Component({
  selector: 'app-all-products',
  templateUrl: './all-products.component.html',
  styleUrls: ['./all-products.component.scss']
})
export class AllProductsComponent implements OnInit {
 
  @Output() sendproductincart = new EventEmitter();
  @Input() categoryproducts;
  
  constructor() { }

  ngOnInit(): void {
  
  }

  userproduct($event){
  
    this.sendproductincart.emit($event);
  }

}
