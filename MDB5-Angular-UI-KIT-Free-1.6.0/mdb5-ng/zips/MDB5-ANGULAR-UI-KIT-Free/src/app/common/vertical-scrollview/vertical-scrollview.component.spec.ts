import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerticalScrollviewComponent } from './vertical-scrollview.component';

describe('VerticalScrollviewComponent', () => {
  let component: VerticalScrollviewComponent;
  let fixture: ComponentFixture<VerticalScrollviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerticalScrollviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerticalScrollviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
