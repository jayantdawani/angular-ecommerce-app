import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-productview',
  templateUrl: './productview.component.html',
  styleUrls: ['./productview.component.scss']
})
export class ProductviewComponent implements OnInit {

  
  
  
  @Input() product;
  @Output() userproductEvent = new EventEmitter();
  userproduct;

  constructor() { 
    
    
    
  }
  
  ngOnInit(): void {
    this.userproduct = this.product;
    
  }
  

  adding(){
    
      this.userproduct.quantity += 1;
      this.userproductEvent.emit(this.userproduct);
     
  }

  subtracting(){
      this.userproduct.quantity += -1;
      this.userproductEvent.emit(this.userproduct);
      
  }
  
 

}
