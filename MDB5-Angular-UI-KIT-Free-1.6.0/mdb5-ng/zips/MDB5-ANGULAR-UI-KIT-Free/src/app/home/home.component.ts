import { Component, OnInit, Input, Output , EventEmitter } from '@angular/core';
import { HomeserviceService} from '../services/homeservice.service';
import { UserService} from '../services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  
  userinfo;
 
  trendingproducts;
  categoriesname;
  totalitems;
  totalprice;
  constructor(private service: UserService){
    
   this.service.currentuserinfo.subscribe(userinfo => this.userinfo = userinfo);
   console.log("test")
   console.log(this.userinfo.cart)
    
  }

  ngOnInit(): void {
      this.trendingproducts = new HomeserviceService().trendingproductsdata();
      this.categoriesname = new HomeserviceService().categoriesnamedata();
      
          
  
  }
 

  receiveproductincart($event){
    

    this.service.userinfodata(this.userinfo);
    let pexist = this.userinfo.cart.includes($event);
    if(pexist==false){
      this.userinfo.cart.push($event);
    }
    if($event.quantity==0){
      let index = this.userinfo.cart.indexOf($event)
      this.userinfo.cart.splice(index, 1);
    }
    console.log("home");
    console.log(this.userinfo.cart);
   
    
    this.totalitems=0;
    this.totalprice=0;

    for (let i = 0 ; i < this.userinfo.cart.length; i++) {
    
      this.totalitems+= this.userinfo.cart[i].quantity;
      this.totalprice+= this.userinfo.cart[i].price * this.userinfo.cart[i].quantity;
     
    }

    this.userinfo.totalprice = this.totalprice;
    this.userinfo.totalitems = this.totalitems;
    console.log("total")
    console.log(this.totalprice)
    console.log(this.totalitems)
    

  }

  

 

}
