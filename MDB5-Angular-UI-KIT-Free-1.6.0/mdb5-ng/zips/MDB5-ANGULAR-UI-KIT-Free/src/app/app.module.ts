import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

// MDB Modules
import { MdbAccordionModule } from 'mdb-angular-ui-kit/accordion';
import { MdbCarouselModule } from 'mdb-angular-ui-kit/carousel';
import { MdbCheckboxModule } from 'mdb-angular-ui-kit/checkbox';
import { MdbCollapseModule } from 'mdb-angular-ui-kit/collapse';
import { MdbDropdownModule } from 'mdb-angular-ui-kit/dropdown';
import { MdbFormsModule } from 'mdb-angular-ui-kit/forms';
import { MdbModalModule } from 'mdb-angular-ui-kit/modal';
import { MdbPopoverModule } from 'mdb-angular-ui-kit/popover';
import { MdbRadioModule } from 'mdb-angular-ui-kit/radio';
import { MdbRangeModule } from 'mdb-angular-ui-kit/range';
import { MdbRippleModule } from 'mdb-angular-ui-kit/ripple';
import { MdbScrollspyModule } from 'mdb-angular-ui-kit/scrollspy';
import { MdbTabsModule } from 'mdb-angular-ui-kit/tabs';
import { MdbTooltipModule } from 'mdb-angular-ui-kit/tooltip';
import { MdbValidationModule } from 'mdb-angular-ui-kit/validation';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SliderComponent } from './common/slider/slider.component';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './home/header/header.component';
import { SearchComponent } from './common/search/search.component';
import { ProductviewComponent } from './common/productview/productview.component';
import { HorizontalScrollviewComponent } from './common/horizontal-scrollview/horizontal-scrollview.component';
import { VerticalScrollviewComponent } from './common/vertical-scrollview/vertical-scrollview.component';
import { HomeCategoriesComponent } from './home/home-categories/home-categories.component';
import { ViewcartComponent } from './common/viewcart/viewcart.component';
import { AllProductsComponent } from './common/all-products/all-products.component';
import { CategoryPageComponent } from './category-page/category-page.component';
import { SubCategoryComponent } from './category-page/sub-category/sub-category.component';
import { CategoryHeaderComponent } from './category-page/category-header/category-header.component';
import { CartPageComponent } from './cart-page/cart-page.component';
import { FinalProductviewComponent } from './cart-page/final-productview/final-productview.component';
import { PaymentviewComponent } from './cart-page/paymentview/paymentview.component';

@NgModule({
  declarations: [
    AppComponent,
    SliderComponent,
    HomeComponent,
    HeaderComponent,
    SearchComponent,
    ProductviewComponent,
    HorizontalScrollviewComponent,
    VerticalScrollviewComponent,
    HomeCategoriesComponent,
    ViewcartComponent,
    AllProductsComponent,
    CategoryPageComponent,
    SubCategoryComponent,
    CategoryHeaderComponent,
    CartPageComponent,
    FinalProductviewComponent,
    PaymentviewComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MdbAccordionModule,
    MdbCarouselModule,
    MdbCheckboxModule,
    MdbCollapseModule,
    MdbDropdownModule,
    MdbFormsModule,
    MdbModalModule,
    MdbPopoverModule,
    MdbRadioModule,
    MdbRangeModule,
    MdbRippleModule,
    MdbScrollspyModule,
    MdbTabsModule,
    MdbTooltipModule,
    MdbValidationModule,
    RouterModule.forRoot([
      {path: 'cart', component: CartPageComponent},
      { path:'category/:categorypath', component: CategoryPageComponent},
      { path:'', component: HomeComponent},
      
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
