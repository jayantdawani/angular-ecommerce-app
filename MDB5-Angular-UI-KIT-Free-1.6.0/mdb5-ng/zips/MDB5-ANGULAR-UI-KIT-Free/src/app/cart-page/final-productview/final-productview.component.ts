import { Component, OnInit , Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-final-productview',
  templateUrl: './final-productview.component.html',
  styleUrls: ['./final-productview.component.scss']
})
export class FinalProductviewComponent implements OnInit {

  @Input() product;
  @Output() userproductEvent = new EventEmitter();
  constructor() { }
  userproduct;
  ngOnInit(): void {
    this.userproduct = this.product;
  }
  adding(){
    
    this.userproduct.quantity += 1;
    this.userproductEvent.emit(this.userproduct);
   
}

subtracting(){
    this.userproduct.quantity += -1;
    this.userproductEvent.emit(this.userproduct);
    
}

}
