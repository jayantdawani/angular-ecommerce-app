import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalProductviewComponent } from './final-productview.component';

describe('FinalProductviewComponent', () => {
  let component: FinalProductviewComponent;
  let fixture: ComponentFixture<FinalProductviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinalProductviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalProductviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
