import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-paymentview',
  templateUrl: './paymentview.component.html',
  styleUrls: ['./paymentview.component.scss']
})
export class PaymentviewComponent implements OnInit {
 
  @Input() totalprice;
  @Input() address;
  constructor() { }

  ngOnInit(): void {
  }

}
