import { Component, OnInit,Input, Output , EventEmitter } from '@angular/core';

@Component({
  selector: 'app-category-header',
  templateUrl: './category-header.component.html',
  styleUrls: ['./category-header.component.scss']
})
export class CategoryHeaderComponent implements OnInit {

  categoryid;
  categorydata;
  @Input() categorypath; 
  @Input() categoriesname;
  @Output() categoryidEvent = new EventEmitter();
  @Output() updatedcategorypath = new EventEmitter();
  categorystatus: boolean = false;
  constructor() { 
    this.categoryidEvent.emit(this.categoryid);

  }

  ngOnInit(): void {
  }
  
  categoryshow(){
    this.categorystatus = !this.categorystatus;
 
  }

  getcategoryproducts($event){
    this.categorypath = $event;
    this.categorystatus = !this.categorystatus;
    console.log( this.categorypath);
    this.updatedcategorypath.emit(this.categorypath)
  }

  

  
  

  

  
}
