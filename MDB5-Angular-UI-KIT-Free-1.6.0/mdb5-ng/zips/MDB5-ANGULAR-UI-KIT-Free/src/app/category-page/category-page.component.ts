import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HomeserviceService} from '../services/homeservice.service';
import { CategoryService} from '../services/category.service';
import { UserService} from '../services/user.service';

@Component({
  selector: 'app-category-page',
  templateUrl: './category-page.component.html',
  styleUrls: ['./category-page.component.scss']
})
export class CategoryPageComponent implements OnInit {
  
  userinfo;
  totalitems;
  totalprice;
  categorypath;
  categoryname;
  categoriesname;
  categoryproducts;
 
  constructor(private route: ActivatedRoute, private service: UserService) { 
    this.categoriesname = new HomeserviceService().categoriesnamedata();

    this.service.currentuserinfo.subscribe(userinfo => this.userinfo = userinfo);
   console.log("test category ")
   console.log(this.userinfo)
    
  }


  ngOnInit(): void {

    this.route.paramMap
      .subscribe(params => {
        this.categorypath = params.get('categorypath');
       
        console.log(this.categorypath);
        
      })
      
      this.categoryproducts = new CategoryService().categorydata(this.categorypath);
      

  }

  updatedcategorypath($event){
    this.categorypath = $event;
    this.categoryproducts = new CategoryService().categorydata(this.categorypath);
   
  
  }
  

  receiveproductincart($event){
    

    this.service.userinfodata(this.userinfo);
    let pexist = this.userinfo.cart.includes($event);
      if(pexist==false){
        this.userinfo.cart.push($event);
      }
    if($event.quantity==0){
      let index = this.userinfo.cart.indexOf($event)
      this.userinfo.cart.splice(index, 1);
    }
    console.log("home");
    console.log(this.userinfo.cart);
   
    
    this.totalitems=0;
    this.totalprice=0;

    for (let i = 0 ; i < this.userinfo.cart.length; i++) {
    
      this.totalitems+= this.userinfo.cart[i].quantity;
      this.totalprice+= this.userinfo.cart[i].price * this.userinfo.cart[i].quantity;
     
    }

    this.userinfo.totalprice = this.totalprice;
    this.userinfo.totalitems = this.totalitems;
    console.log("total")
    console.log(this.totalprice)
    console.log(this.totalitems)
    

  }


  

}
