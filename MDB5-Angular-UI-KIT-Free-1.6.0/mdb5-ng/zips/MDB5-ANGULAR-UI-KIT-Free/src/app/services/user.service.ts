import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService {
 
  
  userinfo= 
  {
        "id":1,
        "name": "jayant",
         "address" :[
           {
             "address1":"shanti nagar damohnaka"
           }
         ],
         "cart":[
            
         ],
         "totalitems":0,
         "totalprice":0
  }
  private defaultuserinfo = new BehaviorSubject(this.userinfo);
  currentuserinfo = this.defaultuserinfo.asObservable();
  
  constructor() { }
 
  getuserinfodata(){
    return this.currentuserinfo;
  }

  userinfodata($cart){
    this.defaultuserinfo.next($cart);
   
  }

  
}
