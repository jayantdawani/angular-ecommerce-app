import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  
  categoryproducts;
  category1 = [
    { 
      "id":100,
      "name": "tomatoes",
      "image": "https://media.istockphoto.com/photos/tomatoes-isolate-on-white-background-tomato-half-isolated-tomatoes-picture-id1258142863?b=1&k=20&m=1258142863&s=170667a&w=0&h=iFVeHatKRUPjoAd2YR1Lgjv_74tZ-gTBbT3cOqFy0BI=",
      "category": "fruits & vegetables",
      "price": 1,
      "weight": "500g",
      "discount": 20,
      "stock":10,
      "quantity":0
    },
    { 
      "id":102,
      "name": "oranges",
      "image": "https://media.istockphoto.com/photos/tomatoes-isolate-on-white-background-tomato-half-isolated-tomatoes-picture-id1258142863?b=1&k=20&m=1258142863&s=170667a&w=0&h=iFVeHatKRUPjoAd2YR1Lgjv_74tZ-gTBbT3cOqFy0BI=",
      "category": "fruits & vegetables",
      "price": 2,
      "weight": "500g",
      "discount": 20,
      "stock":10,
      "quantity":0
    },
    { 
      "id":103,
      "name": "bananas",
      "image": "https://media.istockphoto.com/photos/tomatoes-isolate-on-white-background-tomato-half-isolated-tomatoes-picture-id1258142863?b=1&k=20&m=1258142863&s=170667a&w=0&h=iFVeHatKRUPjoAd2YR1Lgjv_74tZ-gTBbT3cOqFy0BI=",
      "category": "fruits & vegetables",
      "price": 3,
      "weight": "500g",
      "discount": 20,
      "stock":10,
      "quantity":0
    },
    { 
      "id":104,
      "name": "tomatoes",
      "image": "https://media.istockphoto.com/photos/tomatoes-isolate-on-white-background-tomato-half-isolated-tomatoes-picture-id1258142863?b=1&k=20&m=1258142863&s=170667a&w=0&h=iFVeHatKRUPjoAd2YR1Lgjv_74tZ-gTBbT3cOqFy0BI=",
      "category": "fruits & vegetables",
      "price": 1,
      "weight": "500g",
      "discount": 20,
      "stock":10,
      "quantity":0
    },
    { 
      "id":2,
      "name": "oranges",
      "image": "https://media.istockphoto.com/photos/tomatoes-isolate-on-white-background-tomato-half-isolated-tomatoes-picture-id1258142863?b=1&k=20&m=1258142863&s=170667a&w=0&h=iFVeHatKRUPjoAd2YR1Lgjv_74tZ-gTBbT3cOqFy0BI=",
      "category": "fruits & vegetables",
      "price": 2,
      "weight": "500g",
      "discount": 20,
      "stock":10,
      "quantity":0
    },
    { 
      "id":3,
      "name": "bananas",
      "image": "https://media.istockphoto.com/photos/tomatoes-isolate-on-white-background-tomato-half-isolated-tomatoes-picture-id1258142863?b=1&k=20&m=1258142863&s=170667a&w=0&h=iFVeHatKRUPjoAd2YR1Lgjv_74tZ-gTBbT3cOqFy0BI=",
      "category": "fruits & vegetables",
      "price": 3,
      "weight": "500g",
      "discount": 20,
      "stock":10,
      "quantity":0
    }
    
    
   
  ]

  category2 = [
    { 
      "id":200,
      "name": "sun flower ",
      "image": "https://media.istockphoto.com/photos/tomatoes-isolate-on-white-background-tomato-half-isolated-tomatoes-picture-id1258142863?b=1&k=20&m=1258142863&s=170667a&w=0&h=iFVeHatKRUPjoAd2YR1Lgjv_74tZ-gTBbT3cOqFy0BI=",
      "category": "fruits & vegetables",
      "price": 1,
      "weight": "500g",
      "discount": 20,
      "stock":10,
      "quantity":0
    },
    { 
      "id":201,
      "name": "sunflower",
      "image": "https://media.istockphoto.com/photos/tomatoes-isolate-on-white-background-tomato-half-isolated-tomatoes-picture-id1258142863?b=1&k=20&m=1258142863&s=170667a&w=0&h=iFVeHatKRUPjoAd2YR1Lgjv_74tZ-gTBbT3cOqFy0BI=",
      "category": "fruits & vegetables",
      "price": 2,
      "weight": "500g",
      "discount": 20,
      "stock":10,
      "quantity":0
    },
    { 
      "id":203,
      "name": "bananas",
      "image": "https://media.istockphoto.com/photos/tomatoes-isolate-on-white-background-tomato-half-isolated-tomatoes-picture-id1258142863?b=1&k=20&m=1258142863&s=170667a&w=0&h=iFVeHatKRUPjoAd2YR1Lgjv_74tZ-gTBbT3cOqFy0BI=",
      "category": "fruits & vegetables",
      "price": 3,
      "weight": "500g",
      "discount": 20,
      "stock":10,
      "quantity":0
    },
    { 
      "id":204,
      "name": "tomatoes",
      "image": "https://media.istockphoto.com/photos/tomatoes-isolate-on-white-background-tomato-half-isolated-tomatoes-picture-id1258142863?b=1&k=20&m=1258142863&s=170667a&w=0&h=iFVeHatKRUPjoAd2YR1Lgjv_74tZ-gTBbT3cOqFy0BI=",
      "category": "fruits & vegetables",
      "price": 1,
      "weight": "500g",
      "discount": 20,
      "stock":10,
      "quantity":0
    },
    { 
      "id":205,
      "name": "oranges",
      "image": "https://media.istockphoto.com/photos/tomatoes-isolate-on-white-background-tomato-half-isolated-tomatoes-picture-id1258142863?b=1&k=20&m=1258142863&s=170667a&w=0&h=iFVeHatKRUPjoAd2YR1Lgjv_74tZ-gTBbT3cOqFy0BI=",
      "category": "fruits & vegetables",
      "price": 2,
      "weight": "500g",
      "discount": 20,
      "stock":10,
      "quantity":0
    },
    { 
      "id":206,
      "name": "bananas",
      "image": "https://media.istockphoto.com/photos/tomatoes-isolate-on-white-background-tomato-half-isolated-tomatoes-picture-id1258142863?b=1&k=20&m=1258142863&s=170667a&w=0&h=iFVeHatKRUPjoAd2YR1Lgjv_74tZ-gTBbT3cOqFy0BI=",
      "category": "fruits & vegetables",
      "price": 3,
      "weight": "500g",
      "discount": 20,
      "stock":10,
      "quantity":0
    }
    
    
      
  ]
  
  
  categorydata($categorypath){
    
    if($categorypath=="fruits-and-vegetables"){
      return this.category1;
    }

    if($categorypath=="cooking-essentials"){
      return this.category2;
    }
  }


  constructor() { }
}
